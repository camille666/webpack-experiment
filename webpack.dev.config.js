'use strict';
// webpack合并插件
const merge = require('webpack-merge');
const commonWkConfig = require('./webpack.common.config.js');

// 本地开发 Server 配置
const DEV_SERVER_CONFIG = {
    historyApiFallback: true,
    hot: true,
    inline: true,
    progress: true,
    host: '127.0.0.1',
    open: true,
    overlay: true,
    proxy: {
        '/api/*': { // 静态
            target: 'http://127.0.0.1:3005',
            secure: true,
            changeOrigin: true
        },
        /*'/api/*': { // 动态
            target: 'http://127.0.0.1:3003',
            secure: true,
            changeOrigin: true
        }*/
    }
};

// webpack4，新加属性
let OPTIMIZE = {
    splitChunks: {
        chunks: 'initial', // 代码块类型 必须三选一： "initial"（初始化） | "all"(默认就是all) | "async"（动态加载）
        minSize: 0, // 最小尺寸，默认0
        minChunks: 1, // 最小 chunk ，默认1
        maxAsyncRequests: 1, // 最大异步请求数， 默认1
        maxInitialRequests: 1, // 最大初始化请求书，默认1
        name: () => {}, // 名称，此选项课接收 function
        cacheGroups: {
            // 缓存组会继承splitChunks的配置，但是test、priorty和reuseExistingChunk只能用于配置缓存组。
            reactlib: {
                // key 为entry中定义的 入口名称
                chunks: 'initial', // 必须三选一： "initial"(初始化) | "all" | "async"(默认就是异步)
                test: /react/, // 正则规则验证，如果符合就提取 chunk
                name: 'reactlib', // 要缓存的 分隔出来的 chunk 名称
                minSize: 0,
                minChunks: 1,
                enforce: true,
                reuseExistingChunk: true // 可设置是否重用已用chunk 不再创建新的chunk
            },
            antdlib: {
                // key 为entry中定义的 入口名称
                chunks: 'initial', // 必须三选一： "initial"(初始化) | "all" | "async"(默认就是异步)
                test: /antd/, // 正则规则验证，如果符合就提取 chunk
                name: 'antdlib', // 要缓存的 分隔出来的 chunk 名称
                minSize: 0,
                minChunks: 1,
                enforce: true,
                reuseExistingChunk: true // 可设置是否重用已用chunk 不再创建新的chunk
            },
            jquerylib: {
                // key 为entry中定义的 入口名称
                chunks: 'initial', // 必须三选一： "initial"(初始化) | "all" | "async"(默认就是异步)
                test: /jquery/, // 正则规则验证，如果符合就提取 chunk
                name: 'jquerylib', // 要缓存的 分隔出来的 chunk 名称
                minSize: 0,
                minChunks: 1,
                enforce: true,
                reuseExistingChunk: true // 可设置是否重用已用chunk 不再创建新的chunk
            }
        }
    },
    runtimeChunk: {
        name: 'runtime'
    }
};

let devConfig = merge(commonWkConfig, {
    mode: 'development',
    devtool: false,
    devServer: DEV_SERVER_CONFIG,
    optimization: OPTIMIZE
});

module.exports = devConfig;