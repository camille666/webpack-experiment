import React from 'react';
import { render } from 'react-dom';
// import LessExp from '../demos/lessExp/lessExp.jsx';
// import Es6Export from '../demos/es6Export/es6Export.jsx';
// import RequireImport from '../demos/requireImport/requireImport.jsx';
// import FakeData from '../demos/fakeData/asyncDataReadFile/fakeDataAsyncFile.jsx';
// import ReactListA from '../demos/reactList/reactListA.jsx';
// import ReactListB from '../demos/reactList/reactListB.jsx';
import ReactListC from '../demos/reactList/reactListC.jsx';
import ReactListD from '../demos/reactList/reactListD.jsx';
import ReactListE from '../demos/reactList/reactListE.jsx';
import ReactListF from '../demos/reactList/reactListF.jsx';
import ReactListG from '../demos/reactList/reactListG.jsx';

class App extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {}

    render() {
        return (
            <div>
                {/* <LessExp></LessExp> 
                <Es6Export></Es6Export>
                <RequireImport></RequireImport> */}
                {/* <FakeData></FakeData> */}
                {/* <ReactListB></ReactListB> */}
                {/* <ReactListC></ReactListC> */}
                {/* <ReactListD></ReactListD> */}
                {/* <ReactListE></ReactListE> */}
                {/* <ReactListF></ReactListF> */}
                <ReactListG></ReactListG>
            </div>
        );
    }
}

App.defaultProps = {};
render(<App/>, document.getElementById('root'));