import React from 'react';
import * as circle from './circle';
import square from './square';

class Es6Export extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <table>
          <tbody>
            <tr><th>形状</th><th>正方形</th><th>圆</th></tr>
            <tr><td>周长</td><td>--</td><td>{circle.circleLength(2)}</td></tr>
            <tr><td>面积</td><td>{square(2)}</td><td>{circle.circleArea(2)}</td></tr>
          </tbody>
        </table>
      </div>
    );
  }
}

Es6Export.defaultProps = {};
export default Es6Export;
