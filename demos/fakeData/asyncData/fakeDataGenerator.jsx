import React from 'react';

class FakeDataGenerator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: []
    };
  }

  componentDidMount() {
    function * mockAPI(dataArr, time = 1000) {
      yield new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(dataArr);
        }, time);
      });
    }
    
    let self = this;
    let dataArr = [];
    
    for (let i = 1; i <= 5; i++) {
      dataArr.push({'id': i, 'name': Math.random().toString(36).substr(2)});
    }

    let g = mockAPI(dataArr);
    let result = g.next();
    result.value.then(function(res){
      console.log(res);
      console.log(g.next(res));
      console.log(g.next(res).value);
      self.setState({
        listData: res
      });
    });
  }
  
  render() {
    let {listData} = this.state;
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              listData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataGenerator.defaultProps = {};
export default FakeDataGenerator;
