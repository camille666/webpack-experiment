import React from 'react';

class FakeDataAsync extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: []
    };
  }

  componentDidMount() {
    function mockAPI(result, time = 1000) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(result);
        }, time);
      });
    }

    let self = this;
    async function getData() {
      let result = [];
      for (let i = 1; i <= 5; i++) {
        result.push({'id': i, 'name': Math.random().toString(36).substr(2)});
      }
      const r = await mockAPI(result);
      self.setState({
        listData: r
      });
    }

    getData();
  }

  render() {
    let {listData} = this.state;
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              listData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataAsync.defaultProps = {};
export default FakeDataAsync;
