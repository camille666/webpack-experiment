import React from 'react';

class FakeDataPromise extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: []
    };
  }

  componentDidMount() {
    let listData = [
      {
          'id': '001',
          'name': 'banana'
      },
      {
          'id': '002',
          'name': 'orange'
      },
      {
          'id': '003',
          'name': 'apple'
      }
    ];
    
    function mockAPI(url) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(listData);
        }, 300);
      });
    }

    mockAPI('/api/list').then((data) => {
        this.setState({
          listData: data
        });
    });
  }

  render() {
    let {listData} = this.state;
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              listData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataPromise.defaultProps = {};
export default FakeDataPromise;
