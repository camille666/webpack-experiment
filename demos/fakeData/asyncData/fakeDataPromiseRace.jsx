import React from 'react';

class FakeDataPromiseRace extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: []
    };
  }

  componentDidMount() {
    let listArr = [
      {
          'id': '001',
          'name': 'banana'
      },
      {
          'id': '002',
          'name': 'orange'
      },
      {
          'id': '003',
          'name': 'apple'
      }
    ];
    let bookArr = [{
          'id': '0001',
          'name': '绝望锻炼了我'
      },
      {
          'id': '0002',
          'name': '少有人走的路'
      },
      {
          'id': '0003',
          'name': '遇见未知的自己'
      }
    ];
    var req1 = new Promise(function(resolve, reject) { 
        // 通过setTimeout模拟异步任务
        setTimeout(function() { resolve(listArr); }, 8000);
    });
    var req2 = new Promise(function(resolve, reject) { 
        // 通过setTimeout模拟异步任务
        setTimeout(function() { resolve(bookArr); }, 3000);
    });
    const self = this;
    Promise.race([req1, req2]).then(function(one) {
        console.log('Then: ', one);
        self.setState({
          listData: one
        });
    }).catch(function(one, two) {
        console.log('Catch: ', one);
    });
  }

  render() {
    let {listData} = this.state;
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              listData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataPromiseRace.defaultProps = {};
export default FakeDataPromiseRace;
