import React from 'react';

class FakeDataPromiseAllAsync extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: []
    };
  }

  componentDidMount() {
    function mockAPI(result, time = 1000) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(result);
        }, time);
      });
    }
    
    const start = Date.now();
    function worker(tasks, result) {
      const task = tasks.shift();
      if (!task) {
        // 任务结束
        return;
      }
      const costtime = task % 3 === 0 ? 3 : task % 3; // 第一个任务1s， 第二个2是， 第三个3s...
      return mockAPI(task, costtime * 1000).then(r => {
        console.log(`${r} completes at time: ${Date.now() - start}`);
        result.push({'id': r, 'name': Math.random().toString(36).substr(2)});
        return worker(tasks, result);
      });
    }

    let self = this;
    // 处理高并发，并且限流，把所需时间短的任务放一起并发执行。
    async function getData() {
      const t1 = Date.now();
      const total = await mockAPI(5);
      const MAX_CURRENCY = 3;
      const result = [];

      const tasks = [];
      for (let i = 1; i <= total; i += 1) {
        tasks.push(i);
      }
      const promises = [];
      for (let i = 0; i < MAX_CURRENCY; i += 1) {
        promises.push(worker(tasks, result));
      }

      await Promise.all(promises);
      const t2 = Date.now();
      console.log(`total cost: ${t2 - t1}ms.`);
      
      self.setState({
        listData: result
      });
    }
    getData();
  }

  render() {
    let {listData} = this.state;
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              listData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataPromiseAllAsync.defaultProps = {};
export default FakeDataPromiseAllAsync;
