import React from 'react';

const globalData = [
  {
      'id': '001',
      'name': 'banana'
  },
  {
      'id': '002',
      'name': 'orange'
  },
  {
      'id': '003',
      'name': 'apple'
  }
];

class FakeDataGlobal extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
  }

  render() {
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              globalData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataGlobal.defaultProps = {};
export default FakeDataGlobal;
