import React from 'react';

class FakeDataProps extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    let {data} = this.props;
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              data.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataProps.defaultProps = {
  'data': [
      {
          'id': '001',
          'name': 'banana'
      },
      {
          'id': '002',
          'name': 'orange'
      },
      {
          'id': '003',
          'name': 'apple'
      }
  ]
};
export default FakeDataProps;
