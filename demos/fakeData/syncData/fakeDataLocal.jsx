import React from 'react';

class FakeDataLocal extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
  }

  render() {
    let listData = [
      {
          'id': '001',
          'name': 'banana'
      },
      {
          'id': '002',
          'name': 'orange'
      },
      {
          'id': '003',
          'name': 'apple'
      }
    ];
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              listData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataLocal.defaultProps = {};
export default FakeDataLocal;
