import React from 'react';
const path = require('path');
const FILE_PATH = path.resolve(__dirname, './demos/fakeData/asyncDataReadFile');

var flat = function *(arr){ 
  for (var item of arr) {
     if (Array.isArray(item)) {
       yield* flat(item);
     } else {
       yield item;
     }  
  }
};

class FakeDataAsyncFiles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: []
    };
  }

  componentDidMount = async () => {
    var url1 = `${FILE_PATH}/list.json`;
    var url2 = `${FILE_PATH}/books.json`;

    // 处理并发
    const [r1, r2] = await Promise.all([url1, url2].map(url => fetch(url)));
    
    const r1Res = await r1.json();
    let r1ResArr = r1Res.data;

    const r2Res = await r2.json();
    let r2ResArr = r2Res.data;
    let resultArr = r1ResArr.concat(r2ResArr);

    this.setState({
      listData: resultArr
    });
  }

  render() {
    let {listData} = this.state;
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              listData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataAsyncFiles.defaultProps = {};
export default FakeDataAsyncFiles;
