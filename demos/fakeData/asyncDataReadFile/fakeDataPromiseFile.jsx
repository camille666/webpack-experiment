import React from 'react';
const path = require('path');
const FILE_PATH = path.resolve(__dirname, './demos/fakeData/asyncDataReadFile');

// 写一个全局函数
function get(url) {
  console.log(url);
  // 返回一个 promise 对象.
  return new Promise(function(resolve, reject) {
    var req = new XMLHttpRequest();
    req.open('GET', url);
    req.onload = function() {
      if (req.status == 200) {
        resolve(req.response);
      }
      else {
        reject(Error(req.statusText));
      }
    };

    req.onerror = function() {
      reject(Error("网络出错"));
    };

    req.send();
  });
};

class FakeDataPromiseFile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: []
    };
  }

  componentDidMount() {
    let self = this;
    
    get(`${FILE_PATH}/list.json`).then(function(response) {
      console.log("Success!", JSON.parse(response));
      self.setState({
        listData: JSON.parse(response).data
      });
    }, function(error) {
      console.error("Failed!", error);
    });
  }

  render() {
    let {listData} = this.state;
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              listData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataPromiseFile.defaultProps = {};
export default FakeDataPromiseFile;
