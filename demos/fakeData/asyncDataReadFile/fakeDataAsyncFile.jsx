import React from 'react';
const path = require('path');
const FILE_PATH = path.resolve(__dirname, './demos/fakeData/asyncDataReadFile');

class FakeDataAsyncFile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: []
    };
  }

  componentDidMount() {
    async function mockAPI(url) {
      let f = await fetch(url);
      return f;
    }
    
    const self = this;
    let f = mockAPI(`${FILE_PATH}/books.json`);
    f.then(function(response) {
      return response.json(); 
    }).then(function(data) { 
      console.log(data);
      self.setState({
        listData: data.data
      });
    });
  }

  render() {
    let {listData} = this.state;
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              listData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataAsyncFile.defaultProps = {};
export default FakeDataAsyncFile;
