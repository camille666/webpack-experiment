import React from 'react';
const path = require('path');
const FILE_PATH = path.resolve(__dirname, './demos/fakeData/asyncDataReadFile');

class FakeDataGeneratorFile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: []
    };
  }

  componentDidMount() {
    function * mockAPI(url) {
      yield fetch(url);
    }
    
    const self = this;
    let g = mockAPI(`${FILE_PATH}/books.json`);
    let result = g.next();
    result.value.then(function(response) {
      // 这里只能得到response对象
      return response.json(); 
    }).then(function(data) { 
      // 这里得到数据
      console.log(data);
      console.log(g.next(data));
      self.setState({
        listData: data.data
      });
      return g.next(data).value; 
    });
  }

  render() {
    let {listData} = this.state;
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              listData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataGeneratorFile.defaultProps = {};
export default FakeDataGeneratorFile;
