import React from 'react';
const path = require('path');
const FILE_PATH = path.resolve(__dirname, './demos/fakeData/asyncDataReadFile');

var flat = function *(arr) { 
  for (var item of arr) {
     if (Array.isArray(item)) {
       yield* flat(item);
     } else {
       yield item;
     }  
  }
};

class FakeDataPromiseAllFile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: []
    };
  }

  componentDidMount() {
    var request1 = fetch(`${FILE_PATH}/list.json`);
    var request2 = fetch(`${FILE_PATH}/books.json`);

    let self = this;
    // 并发
    Promise.all([request1, request2]).then(function(responses) {
      let tempArr = [];
      responses.map((item, i)=> {
        item.json().then(function(data) {
          tempArr.push(data.data);
          if (i == 1) {
            let result = [...(flat(tempArr))];
            self.setState({
              listData: result
            });
          }
        });
      });
    });
  }

  render() {
    let {listData} = this.state;
    
    return (
      <div>
        <table>
          <tbody>
            <tr><th>id</th><th>name</th></tr>
            {
              listData.map((item, i) => {
                return <tr key={i}><td>{item.id}</td><td>{item.name}</td></tr>
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

FakeDataPromiseAllFile.defaultProps = {};
export default FakeDataPromiseAllFile;
