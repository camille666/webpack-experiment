import React from 'react';
import './lessExp.css';
import styles from './lessModuleExp.less';

class LessExp extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    return (
      <div className='box'>
        <p className={styles.pfont}>测试cssmodule语法</p>
      </div>
    );
  }
}

LessExp.defaultProps = {};
export default LessExp;
