import React, {Component, Fragment} from 'react';
import shortid from 'shortid';
import _ from 'lodash';

// 这个例子是正面教材
class Item extends Component {
  
  render() {
    return (
      <div className="form-group" style={{display: 'table'}}>
        <label style={{display: 'table-cell', paddingRight: '5px', 'width': '130px'}}>{this.props.id}</label>
        <div style={{display: 'table-cell', paddingTop: '10px'}}>
          <input type='text' className='form-control' />
        </div>
        <div style={{display: 'table-cell', paddingLeft: '5px'}}>
          <button onClick={_.debounce(this.props.handleAdd, 200)}>添加</button>&nbsp;&nbsp;
          <button onClick={this.props.handleDelete}>删除</button>
        </div>
      </div>
    );
  }
}

class ReactListG extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [
        {timestamp: 1444610101010, id: 1444610101010},
        {timestamp: 1444600000000, id: 1444600000000}
      ]
    };
  }

  // 添加
  handleAdd(index) {
    let {list} = this.state;
    let nextId = shortid.generate();
    const newList = _.cloneDeep(list);
    newList.splice(index + 1, 0, {id: nextId});
    this.setState({
        list: newList
    });
  }

  // 删除
  handleDelete(id) {
    let {list} = this.state;
  
    const newList = [
      ...list
    ];
    for (let index in newList) {
      if (id === newList[index].id) {
        newList.splice(index, 1);
      }
    }
    this.setState({
        list: newList
    });
  }

  render() {
      return (
        <Fragment>
          <h3>Better <code>key=id</code></h3>
          <div>
              {this.state.list.map((value, index) =>
                <Item {...value} key={value.id} handleDelete={this.handleDelete.bind(this, value.id)} handleAdd={this.handleAdd.bind(this, index)}/>
              )}
          </div>
        </Fragment>
      );
  }
}

ReactListG.defaultProps = {};
export default ReactListG;
