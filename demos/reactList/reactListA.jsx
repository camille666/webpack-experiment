import React, {Component, Fragment} from 'react';

class ReactListA extends Component {
  constructor(props){
    super(props);
    this.state = {
        list: [
            'learn react',
            'learn english',
            'learn vue'
        ],
        inputValue: ''
    };
  }

  // 添加
  handleAdd(){
      this.setState({
          list: [...this.state.list, this.state.inputValue],
          inputValue: ''
      });
  }

  // 监听文本框变化
  handleInputChange(e){
      this.setState({
          inputValue: e.target.value
      });
  }

  // 删除
  handleDelete(index){
      console.log(index);
      const list = [...this.state.list];
      list.splice(index, 1);
      this.setState({
          list: list
      });
  }

  render() {
      return (
          <Fragment>
              <input onChange={this.handleInputChange.bind(this)}
              value={this.state.inputValue}
              />
              <button onClick={this.handleAdd.bind(this)}>添加</button>
              <ul>
                {
                    this.state.list.map((item, index)=>{
                        return (
                        <li key={index}>{item}
                          <span style={{marginLeft: '20px'}}
                                onClick={this.handleDelete.bind(this, index)}
                          >删除</span>
                        </li>);
                    })
                }
              </ul>
          </Fragment>
      );
  }
}

ReactListA.defaultProps = {};
export default ReactListA;
