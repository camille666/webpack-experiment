import React, {Component, Fragment} from 'react';
import shortid from 'shortid';
import _ from 'lodash';

// 这个例子是正面教材
class Item extends Component {
  render() {
    
    return (
      <div className="form-group" style={{display: 'table'}}>
        <label style={{display: 'table-cell', paddingRight: '5px', 'width': '130px'}}>{this.props.id}</label>
        <div style={{display: 'table-cell', paddingTop: '10px'}}>
          <input type='text' className='form-control' />
        </div>
        <button style={{display: 'table-cell', paddingLeft: '5px'}} onClick={this.props.handleDelete}>删除</button>
      </div>
    );
  }
}

class ReactListF extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      list: [
        {timestamp: 1444610101010, id: 1444610101010},
        {timestamp: 1444600000000, id: 1444600000000}
      ]
    };
  }

  // 开头添加
  addAtStart() {
    const date = +new Date();
    let nextId = shortid.generate();
    const newList = [
      {id: nextId, timestamp: date},
      ...this.state.list
    ];
    this.setState({
      list: newList
    });
  }

  // 末尾添加
  addAtEnd() {
    const date = +new Date();
    let nextId = shortid.generate();
    const newList = _.cloneDeep(this.state.list);
    newList.push({id: nextId, timestamp: date});
    this.setState({
      list: newList
    });
  }

  // 删除
  handleDelete(id) {
    let {list} = this.state;
  
    const newList = [
      ...list
    ];
    for (let index in newList) {
      if (id === newList[index].id) {
        newList.splice(index, 1);
      }
    }
    this.setState({
        list: newList
    });
  }

  render() {
      return (
        <Fragment>
          <table>
            <tbody>
              <tr>
                <td><button onClick={this.addAtStart.bind(this)}>开头添加</button>&nbsp;&nbsp;</td>
                <td><button onClick={this.addAtEnd.bind(this)}>末尾添加</button></td>
              </tr>
            </tbody>
          </table>
          
          <h3>Better <code>key=id</code></h3>
          <form className="form-horizontal">
              {this.state.list.map((value) =>
                <Item {...value} key={value.id} handleDelete={this.handleDelete.bind(this, value.id)}/>
              )}
          </form>
        </Fragment>
      );
  }
}

ReactListF.defaultProps = {};
export default ReactListF;
