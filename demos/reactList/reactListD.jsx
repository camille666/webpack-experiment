import React, {Component, Fragment} from 'react';

// 这个例子是正面教材
class Item extends Component {
  render() {
    return (
      <div className="form-group" style={{display: 'table'}}>
        <label style={{display: 'table-cell', paddingRight: '5px', 'width': '130px'}}>{this.props.id}</label>
        <div style={{display: 'table-cell', paddingTop: '10px'}}>
          <input type='text' className='form-control' />
        </div>
      </div>
    );
  }
}

class ReactListD extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      list: [
        {timestamp: 1444610101010, id: 1444610101010},
        {timestamp: 1444600000000, id: 1444600000000}
      ],
      activeIndex: ''
    };
  }

  userInput(e) {
    this.setState({
      activeIndex: e.target.value
    });
  }

  // 开头添加
  addAtStart() {
    const date = +new Date();
    let nextId = date;
    const newList = [
      {id: nextId, timestamp: date},
      ...this.state.list
    ];
    this.setState({
      list: newList
    });
  }

  // 中间添加
  addAtMiddle() {
    let {activeIndex, list} = this.state;
    const date = +new Date();
    let nextId = date;
    const newList = [
      ...list
    ];
    
    // 限制中间添加
    if (!!activeIndex && activeIndex < newList.length - 1) {
      newList.splice(activeIndex, 0, {id: nextId, timestamp: date});
      this.setState({
        list: newList
      });
    }
  }

  // 末尾添加
  addAtEnd() {
    const date = +new Date();
    let nextId = date;
    const newList = [
      ...this.state.list,
      {id: nextId, timestamp: date}
    ];
    this.setState({
      list: newList
    });
  }

  // 开头删除
  deleteAtStart() {
    let { list } = this.state;
    
    list.shift();
    this.setState({
        list
    });
  }

  // 中间删除
  deleteAtMiddle() {
    let {activeIndex, list} = this.state;
  
    const newList = [
      ...list
    ];
    // 限制中间添加
    if (!!activeIndex && activeIndex < newList.length - 1) {
      newList.splice(activeIndex, 1);
    }
    this.setState({
        list: newList
    });
  }

  // 末尾删除
  deleteAtEnd() {
    let { list } = this.state;
    
    list.pop();
    this.setState({
        list
    });
  }

  // 最早排序
  sortByEarliest() {
    const sortedList = this.state.list.sort((a, b) => {
      return a.timestamp - b.timestamp;
    });
    this.setState({
      list: [...sortedList]
    });
  }

  // 最晚排序
  sortByLatest() {
    const sortedList = this.state.list.sort((a, b) => {
      return b.timestamp - a.timestamp;
    });
    this.setState({
      list: [...sortedList]
    });
  }

  render() {
      return (
        <Fragment>
          <table>
            <tbody>
              <tr>
                <td><button onClick={this.addAtStart.bind(this)}>开头添加</button>&nbsp;&nbsp;</td>
                <td><input type='text'  onChange={this.userInput.bind(this)}
                value={this.state.activeIndex}/><button onClick={this.addAtMiddle.bind(this)}>中间添加</button>&nbsp;&nbsp;</td>
                <td><button onClick={this.addAtEnd.bind(this)}>末尾添加</button></td>
              </tr>
              <tr>
                <td><button onClick={this.deleteAtStart.bind(this)}>开头删除</button>&nbsp;&nbsp;</td>
                <td><input type='text'  onChange={this.userInput.bind(this)}
                value={this.state.activeIndex}/><button onClick={this.deleteAtMiddle.bind(this)}>中间删除</button>&nbsp;&nbsp;</td>
                <td><button onClick={this.deleteAtEnd.bind(this)}>末尾删除</button></td>
              </tr>
              <tr>
                <td><button onClick={this.sortByEarliest.bind(this)}>最早排序</button></td>
                <td><button onClick={this.sortByLatest.bind(this)}>最晚排序</button></td>
              </tr>
            </tbody>
          </table>
          
          <h3>Better <code>key=id</code></h3>
          <form className="form-horizontal">
              {this.state.list.map((value) =>
                <Item {...value}
                key={value.id} />
              )}
          </form>
        </Fragment>
      );
  }
}

ReactListD.defaultProps = {};
export default ReactListD;
