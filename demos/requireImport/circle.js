export function circleLength(r) {
    return 2 * Math.PI * r;
}

let temp = ['apple', 'banana'];

function circleArea(r) {
    console.log('temp:', temp);
    return Math.PI * r * r;
};

module.exports = {
    circleLength,
    circleArea
};