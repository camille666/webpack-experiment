'use strict';
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin'); //压缩css插件

// 路径
const ROOT_PATH = path.resolve(__dirname);
const NODE_MODULES = path.resolve(__dirname, './node_modules');
const DEMO_PATH = path.resolve(__dirname, './demos');
const OUTPUT_PATH = path.resolve(__dirname, './build');
const SOURCE_PATH = path.resolve(__dirname, './src');
const ASSETS_PATH = path.resolve(__dirname, './build/assets');
const ASSETS_FOLDER = '/assets';

// 样式分离
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
let miniCssExtractPlugin = new MiniCssExtractPlugin();

// 资源文件
let copyWebpackPlugin = new CopyWebpackPlugin([
    // 复制所有当前工程的 assets 到 output 目录
    {
        context: SOURCE_PATH,
        from: 'assets/',
        to: ASSETS_PATH + '/'
    }
]);
console.log(copyWebpackPlugin);

// 入口文件配置
const ENTRIES_CONFIG = {
    main: SOURCE_PATH + '/index.jsx'
};

// 输出配置
const OUTPUT_CONFIG = {
    path: OUTPUT_PATH,
    filename: '[name].js',
    chunkFilename: '[name].bundle.[chunkHash:5].js'
};

// 处理的文件类型和范围
const RESOLVE_CONFIG = {
    extensions: ['.js', '.jsx', '.json'],
    modules: ['node_modules', SOURCE_PATH],
    alias: {
        assets: path.resolve(SOURCE_PATH, 'assets/')
    }
};

// 插件
const PLUGINS = [
    miniCssExtractPlugin,
    // 在编译之前，把旧的编译文件清空，否则会持续生成不同hash的文件，我们只需要保留最新编译的文件。
    new CleanWebpackPlugin(),
    new webpack.SourceMapDevToolPlugin({ columns: false }),
    // 生成index.html胶水代码
    new HtmlWebpackPlugin({
        title: '功能性组件',
        filename: 'index.html',
        template: ROOT_PATH + '/index.html',
        inject: true,
        hash: true
    }),
    new OptimizeCssAssetsPlugin(),
    // 热模块更新
    new webpack.HotModuleReplacementPlugin()
];

// 加载器
const MODULE_RULES = [{
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
    },
    // 自己写的less
    {
        test: /\.less$/,
        exclude: NODE_MODULES,
        use: [
            MiniCssExtractPlugin.loader,
            {
                loader: 'css-loader',
                options: {
                    minimize: false,
                    modules: true,
                    localIdentName: '[name]_[local]_[hash:base64:5]'
                }
            },
            'less-loader'
        ]
    },
    // 包含antd的less
    {
        test: /\.less$/,
        include: NODE_MODULES,
        use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            { loader: 'less-loader', options: { javascriptEnabled: true } }
        ]
    },
    {
        test: /\.jsx?$/,
        include: /src|demos|node_modules/,
        options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
            plugins: ['@babel/plugin-proposal-object-rest-spread']
        },
        loader: 'babel-loader'
    },
    {
        test: /\.(ttf|eot|svg|png|gif|jpg|jpeg|swf)$/,
        exclude: /node_modules/,
        include: /assets/,
        loader: 'file-loader',
        options: {
            name: function(file) {
                return ASSETS_FOLDER + '/[path][name].[ext]';
            },
            context: 'src/assets/'
        }
    }
];

module.exports = {
    entry: ENTRIES_CONFIG,
    output: OUTPUT_CONFIG,
    resolve: RESOLVE_CONFIG,
    plugins: PLUGINS,
    module: { rules: MODULE_RULES }
};